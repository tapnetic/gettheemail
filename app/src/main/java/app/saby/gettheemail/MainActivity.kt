package app.saby.gettheemail

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import com.google.firebase.auth.ktx.auth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import com.google.firebase.database.ktx.database
import com.google.firebase.database.ktx.getValue
import com.google.firebase.ktx.Firebase

class MainActivity : AppCompatActivity(R.layout.activity_main) {

    private val tvTest: TextView?
        get() = findViewById(R.id.tvTest)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        getTestData()
    }

    private fun getTestData() {
        val userId = Firebase.auth.currentUser?.uid
        if (userId == null) {
            Firebase.auth.signInAnonymously()
                .addOnSuccessListener { result ->
                    if (result.user?.uid != null) {
                        getTestData()
                    }
                }
                .addOnFailureListener { tvTest?.text = it.message }
            return
        }
        Firebase.database.getReference("test")
            .addListenerForSingleValueEvent(object : ValueEventListener {
                override fun onDataChange(snapshot: DataSnapshot) {
                    val testText = snapshot.getValue<String>() ?: "No data"
                    tvTest?.text = testText
                }

                override fun onCancelled(error: DatabaseError) {
                    tvTest?.text = "Error"
                }
            })
    }
}